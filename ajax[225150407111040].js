const button = document.querySelector("#submit");

button.addEventListener("click", async (event) => {
  event.preventDefault();

  const nama = document.getElementById("nama").value;
  const jenisKelamin = document.querySelector('input[name="jenis_kelamin"]:checked').value;
  const tempatTinggal = document.getElementById("tempat_tinggal").value;

  const url = `http://localhost/pemweb/tugasTiga/ajax.php?nama=${nama}&jenisKelamin=${jenisKelamin}&tempatTinggal=${tempatTinggal}`;
  axios.get(url).then((response) => {
    alert(response.data);
  });

});